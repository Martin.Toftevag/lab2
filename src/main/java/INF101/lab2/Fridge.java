package INF101.lab2;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;


public class Fridge implements IFridge{
    
    int maxSize = 20;
    ArrayList<FridgeItem> items = new ArrayList<FridgeItem>();

    @Override
    public int totalSize() {
        return maxSize;
    }

    @Override
    public int nItemsInFridge() {
        
        return items.size();
    }

    @Override
    public boolean placeIn(FridgeItem item) {
        if(items.size() < maxSize) {
            items.add(item);
            return true;
        }

        else {
            return false;
        }
    }
    @Override
    public void takeOut(FridgeItem item) {
        if(items.contains(item)){
            items.remove(item);
        }
        else{
            throw new NoSuchElementException();
        }
    }

    @Override
    public void emptyFridge() {
        items.clear();
    }

    @Override
    public List<FridgeItem> removeExpiredFood() {
        ArrayList<FridgeItem> expitems = new ArrayList<FridgeItem>();
        for(FridgeItem item : items){
            if(item.hasExpired()){
                expitems.add(item);
            }
        }
        for(FridgeItem item : expitems){
            items.remove(item);
        }
        
        return expitems;
    }

}
